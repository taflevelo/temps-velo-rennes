Temps Vélo - a bike travel time map
===================================

This is a map for Rennes area.
If you want to fix something for this specific area, feel free to open a merge request here.

If you want to contribute to the project, please visit [this repository](https://gitlab.com/taflevelo/temps-velo/).
